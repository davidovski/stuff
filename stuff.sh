#!/bin/sh
[ $# -eq 0 ] && exit 1
STUFF_FILE=/usr/share/stuff/stuff.png
FILE=/tmp/$(date "+%Y-%m-%d-%H-%M-%S").png
LABEL_FILE=$FILE.label.png
text=$@

convert -gravity center -background white -fill black -pointsize 80 -size 500x caption:"$text" $LABEL_FILE
convert -gravity center -background white $LABEL_FILE $STUFF_FILE +append $FILE 
xclip -selection clipboard -t image/png -i $FILE

