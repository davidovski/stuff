install: stuff.sh images
	mkdir /usr/share/stuff
	cp images/* /usr/share/stuff/
	cp stuff.sh /usr/bin/stuff
	chmod +x /usr/bin/stuff
	cp chungus.sh /usr/bin/chungus
	chmod +x /usr/bin/chungus
	cp eminem.sh /usr/bin/eminem
	chmod +x /usr/bin/eminem
clean:
	rm -r /usr/share/stuff
	rm /usr/bin/stuff
	rm /usr/bin/chungus
	rm /usr/bin/eminem
